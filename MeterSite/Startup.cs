﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MeterSite.Startup))]
namespace MeterSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
