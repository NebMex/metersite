﻿(function ($) {
    $(function () {
        var scroll = $(document).scrollTop();
        var headerHeight = $('.navbar').outerHeight();

     
        $(window).scroll(function () {
            var scrolled = $(document).scrollTop();
            if (scrolled > headerHeight) {
                $('.navbar').addClass('off-canvas');
            } else {
                $('.navbar').removeClass('off-canvas');
            }

            if (scrolled > scroll) {
                $('.navbar').removeClass('fixed');
            } else {
                $('.navbar').addClass('fixed');
            }
            scroll = $(document).scrollTop();
        });
    });
})(jQuery);
