namespace MeterSite.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MeterSite.Models.MeterSiteContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "MeterSite.Models.MeterSiteContext";
        }

        protected override void Seed(MeterSite.Models.MeterSiteContext context)
        {

            //362dc41e-dcc6-4708-8dc0-b6ad23b12f11
            context.UserInfoes.AddOrUpdate(
                p => p.Id,
                new Models.UserInfo
                {
                    Id = 1,
                    Birthday = new DateTime(1985, 03, 22),
                    City = "Lincoln",
                    Email = "cisneros24@gmail.com",
                    FirstName = "Jeff",
                    LastName = "Cisneros",
                    PhonePrimary = "402-853-2628",
                    State = "NE",
                    StreetAddress = "621 W. Dilin St.",
                    UserId = new Guid("362dc41e-dcc6-4708-8dc0-b6ad23b12f11"),
                    Zip = "68521"
                });
               }
    }
}
