﻿using System.Web;
using System.Web.Optimization;

namespace MeterSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //jquery bundle
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/js/lib/jquery.min.js",
            "~/js/lib/JavaScript.min.js"));


            //modernizr bundle 
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
           "~/Scripts/modernizr.2.8.3.min.js"));
            
            //bootstrap bundles
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/js/lib/bootstrap.min.js",
                      "~/js/lib/bootstrap.offcanvas.min.js"));

            //jquerUI bundle
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/js/lib/jquery-ui-1.12.1.min.js"));

            //css bundles
            bundles.Add(new StyleBundle("~/content/css").Include(
                    "~/css/bootstrap-theme.min.css",
                      "~/css/bootstrap.min.css",
                      "~/css/normalize.min.css",
                      "~/css/bootstrap.offcanvas.min.css",
                      "~/css/jquery-ui.min.css",
                      "~/css/jquery-ui.structure.min.css",
                      "~/css/font-awesome.min.css",
                      "~/css/SiteStyleSheet.min.css"));

           
        }
    }
}
